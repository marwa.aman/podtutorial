# PodTutorial

[![CI Status](https://img.shields.io/travis/Marwa Aman/PodTutorial.svg?style=flat)](https://travis-ci.org/Marwa Aman/PodTutorial)
[![Version](https://img.shields.io/cocoapods/v/PodTutorial.svg?style=flat)](https://cocoapods.org/pods/PodTutorial)
[![License](https://img.shields.io/cocoapods/l/PodTutorial.svg?style=flat)](https://cocoapods.org/pods/PodTutorial)
[![Platform](https://img.shields.io/cocoapods/p/PodTutorial.svg?style=flat)](https://cocoapods.org/pods/PodTutorial)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PodTutorial is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PodTutorial'
```

## Author

Marwa Aman, marwa.m.aman@gmail.com

## License

PodTutorial is available under the MIT license. See the LICENSE file for more info.
